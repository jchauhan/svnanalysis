/**
* This is an extension Project log visualizer using 'Gource' tool
* Modifications:
* 	* Status of current task
*   * Proper log generator
*   * Removing intermediate log after video creation
* Parameters : inputFile authorName
* Syntax: java ReadAndPrintXMLFile sample.xml sampleAuthor
* @author  Aditya
* @version 1.4 
*/
import java.io.File;
import java.io.FileWriter;
import java.util.Date;
import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import java.text.SimpleDateFormat;
import java.text.ParseException;
public class ReadAndPrintXMLFile{
	public static boolean flag=false;

	static long Date_Converter(String s)throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date dt=sdf.parse(s);
		return dt.getTime();
		}
    
    public static void main (String argv []) throws Exception{
    try {
			FileWriter output_file=new FileWriter("output.log");
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse (new File(argv[0]));
            
			String user=argv[1];
            NodeList listOfPersons = doc.getElementsByTagName("logentry");
            
            for(int s=0; s<listOfPersons.getLength(); s++){
            
                Node firstPersonNode = listOfPersons.item(s);
            
                if(firstPersonNode.getNodeType() == Node.ELEMENT_NODE){

                    Element firstPersonElement = (Element)firstPersonNode;
					// Author name
                    NodeList AuthorNameList = firstPersonElement.getElementsByTagName("author");
                    Element Author = (Element)AuthorNameList.item(0);

                    NodeList AuthorName = Author.getChildNodes();
                    String author_name=((Node)AuthorName.item(0)).getNodeValue().trim();
                    
                    if((author_name).equals(user)){
						// Date 
						NodeList DatesList = firstPersonElement.getElementsByTagName("date");
						Element Modified_Date = (Element)DatesList.item(0);
						NodeList Modified_Date_List = Modified_Date.getChildNodes();
						String new_date = ((Node)Modified_Date_List.item(0)).getNodeValue().trim();
						long mill = Date_Converter(new_date);
						mill/=1000;
						// Paths
						NodeList PathsList = firstPersonElement.getElementsByTagName("path");
						for(int j=0;j<PathsList.getLength();j++){
							Element Path_Entry = (Element)PathsList.item(j);
							String val = Path_Entry.getAttribute("action");
							NodeList File_Path = Path_Entry.getChildNodes();							
							String File_Path1 = ((Node)File_Path.item(0)).getNodeValue().trim();
							String data=Long.toString(mill)+"|"+author_name+"|"+val+"|"+File_Path1+"|\n";
							output_file.write(data);
							}
						flag=true;
						}
					}
				}
				if(flag){
					output_file.close();
					try{            
						Runtime rt = Runtime.getRuntime();
						Process proc = rt.exec("gource -640x480 -o gource.ppm --log-format custom output.log");
						System.out.println("Visualizing the log for Author : "+user);
						proc.waitFor();
						proc = rt.exec("rm -v output.log");
						proc.waitFor();
						String cmnd="ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i gource.ppm -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 1 -threads 0 -bf 0 "+user+".mp4";
						proc = rt.exec(cmnd);
						System.out.println("Converting the video to mp4 format..");
						proc.waitFor();
						System.out.println("Deleting the intermediate file..");
						proc = rt.exec("rm -v gource.ppm");
						proc.waitFor();
						System.out.println("Done..!");
						}
					catch (Throwable t){
						t.printStackTrace();
						}
					}
				else{
					System.out.println("'"+argv[1]+"' is not found as author in the specified log file..!");
					}
        }catch (SAXParseException err) {
			System.out.println ("** Parsing error" + ", line " + err.getLineNumber () + ", uri " + err.getSystemId ());
			System.out.println(" " + err.getMessage ());

        }catch (SAXException e) {
			Exception x = e.getException ();
				((x == null) ? e : x).printStackTrace ();
        }catch (Throwable t) {
			t.printStackTrace ();
        }
    }
}