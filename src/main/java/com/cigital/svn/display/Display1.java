package com.cigital.svn.display;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import com.cigital.svn.model.Authors;
import com.cigital.svn.model.Commit;
import com.cigital.svn.model.Diff;
/**
 * 
 * @author bala
 *
 */
public class Display1 {
	/*
	public void Display1(ArrayList<Authors> authorsList,String authorName){
		Iterator<Authors> iterateAuthors=authorsList.iterator();
		while(iterateAuthors.hasNext()){
			Authors tempP=iterateAuthors.next();
			if(tempP.getName().equals(authorName)){
				System.out.println("Author Name::"+tempP.getName());
				Iterator<Commit> tempC=tempP.getCommits().iterator();
				while(tempC.hasNext()){
					Commit tempC1=tempC.next();
					//System.out.println(tempC1.getRefno());
					//System.out.println(tempC1.getTimes());
					Iterator<Diff> tempD=tempC1.getDifff().iterator();
					while(tempD.hasNext()){
						Diff tempD1=tempD.next();
						System.out.println("File Path:: "+tempD1.getPath());
						System.out.println("Diff:: "+tempD1.getCode());
						}
					}
				
				}
			}
	}*/
	public void Display1(ArrayList<Authors> authorsList,String authorName,String st,String et) throws ParseException{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		System.out.println(authorName);
		
		java.util.Date startDate=null;
		if(st!=null)
		startDate= dateFormat.parse(st);
		java.util.Date endDate=null;
		if(et!=null)
		endDate= dateFormat.parse(et);
		Iterator<Authors> iterateAuthors=authorsList.iterator();
		while(iterateAuthors.hasNext()){
			Authors tempP=iterateAuthors.next();
			if(tempP.getName().equals(authorName)||authorName==null){
				System.out.println("Author Name::"+tempP.getName());
				Iterator<Commit> tempC=tempP.getCommits().iterator();
				while(tempC.hasNext()){
					Commit tempC1=tempC.next();
					//System.out.println(tempC1.getRefno());
					//System.out.println(tempC1.getTimes());
					String date=tempC1.getTimes();
					java.util.Date presentDate= dateFormat.parse(date);
					if(st==null&&et==null){
						Iterator<Diff> tempD=tempC1.getDifff().iterator();
						while(tempD.hasNext()){
							Diff tempD1=tempD.next();
							System.out.println("File Path:: "+tempD1.getPath());
							System.out.println("Diff:: "+tempD1.getCode());
							}
						}
					else if(st==null){
						if(presentDate.before(endDate)||(et.equals(date))){
							Iterator<Diff> tempD=tempC1.getDifff().iterator();
							while(tempD.hasNext()){
								Diff tempD1=tempD.next();
								System.out.println("File Path:: "+tempD1.getPath());
								System.out.println("Diff:: "+tempD1.getCode());
								}
							}
					}
					else if(et==null){
						if(startDate.before(presentDate)||(st.equals(date))){
							Iterator<Diff> tempD=tempC1.getDifff().iterator();
							while(tempD.hasNext()){
								Diff tempD1=tempD.next();
								System.out.println("File Path:: "+tempD1.getPath());
								System.out.println("Diff:: "+tempD1.getCode());
								}
							}
					}
					else if(startDate.before(presentDate)&&presentDate.before(endDate)||(st.equals(date))||(et.equals(date))){
						Iterator<Diff> tempD=tempC1.getDifff().iterator();
						while(tempD.hasNext()){
							Diff tempD1=tempD.next();
							System.out.println("File Path:: "+tempD1.getPath());
							System.out.println("Diff:: "+tempD1.getCode());
							}
						}
					}
				}
			}
		
	}
	/*
	public void Display1(ArrayList<Authors> authorsList,String st,String et) throws ParseException{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date startDate= dateFormat.parse(st);
		java.util.Date endDate= dateFormat.parse(et);
		System.out.println(st+" "+et);
		Iterator<Authors> iterateAuthors=authorsList.iterator();
		while(iterateAuthors.hasNext()){
			Authors tempP=iterateAuthors.next();
				Iterator<Commit> tempC=tempP.getCommits().iterator();
				while(tempC.hasNext()){
					Commit tempC1=tempC.next();
					//System.out.println(tempC1.getRefno());
					//System.out.println(tempC1.getTimes());
					String date=tempC1.getTimes();
					java.util.Date presentDate= dateFormat.parse(date);
					if((startDate.before(presentDate)&&presentDate.before(endDate)) ||(st.equals(date))||(et.equals(date))){
						Iterator<Diff> tempD=tempC1.getDifff().iterator();
						while(tempD.hasNext()){
							Diff tempD1=tempD.next();
							System.out.println("Author Name::"+tempP.getName());
							System.out.println("File Path:: "+tempD1.getPath());
							System.out.println("Diff:: "+tempD1.getCode());
							}
						}
					}
			}
		
	}*/
}