/**
 * File: Visualize.java
 * Author: Aditya
 * Description: This file retrieves the commits of given user from the model
 *      and filters the log by start date and end date and visualize output
 *      log with gource tool. It will record the video and report the user 
 *      in mp4 format.
 */
package com.cigital.svn.display.visualize;

import java.io.FileWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;

import com.cigital.svn.model.Authors;
import com.cigital.svn.model.Commit;
import com.cigital.svn.model.Diff;
import com.cigital.svn.model.Diff.Type;

public class Visualize{
	// Date Converter from date to milliseconds
	long dateConverter(String s)throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dt=sdf.parse(s);
		return (dt.getTime()/1000);
		}
	// Readers the filtered log and writes to ouput.log file
	void writeLogToFile(ArrayList<String> A,FileWriter outputFile)throws Exception{
		
		for(int i=0;i<A.size();i++){
			outputFile.write(A.get(i));
			}
		}
	// this method executes the runtime commands to visualize the log and record the video
	void visualizeData(String authorName,String sdate,String edate){
		try{            
			Runtime rt = Runtime.getRuntime();
			Process proc = rt.exec("gource -640x480 -o gource.ppm --fullscreen --log-format custom output.log");
			System.out.println("Visualizing the log");
			proc.waitFor();
			proc = rt.exec("rm -v output.log");
			proc.waitFor();
			String filename="gource-video-startdate("+sdate.substring(0,10)+")-end("+edate.substring(0,10)+")-author-all.mp4";
			if(authorName!=null){	filename="gource-video-start("+sdate.substring(0,10)+")-enddate("+edate.substring(0,10)+")-"+authorName+".mp4";}
			String cmnd="ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i gource.ppm -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 1 -threads 0 -bf 0 "+filename;
			proc = rt.exec(cmnd);
			System.out.println("Converting the video to mp4 format..");
			proc.waitFor();
			System.out.println("Deleting the intermediate file..");
			proc = rt.exec("rm -v gource.ppm");
			proc.waitFor();
			System.out.println("Done..!");
			}
		catch (Throwable t){
			t.printStackTrace();
			}
		}
	// filters required log from complete log by start date and end date and stores the logs in List
	ArrayList<String> generateLog(ArrayList<Commit> commits,long sd,long ed,String authorName)throws Exception{
		ArrayList<String> logLines = new ArrayList<String>();
		Iterator<Commit> tempC=commits.iterator();
		while(tempC.hasNext()){
			Commit tempC1=tempC.next();
			long time = dateConverter(tempC1.getTimes());
			if(time>=sd && time<=ed){
				Iterator<Diff> tempD=tempC1.getDifff().iterator();
				while(tempD.hasNext()){
					Diff tempD1=tempD.next();
					String path = tempD1.getPath();
					String act="";
					Type action = tempD1.getType();
					if(action==Type.add){	act="A";}
					else if(action==Type.delete){	act="D";}
					else if(action==Type.update){	act="M";}
					String line=Long.toString(time)+"|"+authorName+"|"+act+"|"+path+"|\n";
					logLines.add(line);
					}
				}
			}
		return logLines;		
		}
	// main method
	public void visualizePerformance(ArrayList<Authors> authorsList,String authorName,String startDate,String endDate)throws Exception{
		try {
			FileWriter outputFile=new FileWriter("output.log");
			long sDate,eDate;
			sDate=dateConverter(startDate);
			eDate=dateConverter(endDate);
			Iterator<Authors> iterateAuthors=authorsList.iterator();
			ArrayList<String> completeLog=new ArrayList<String>();
			boolean flag=false;
			while(iterateAuthors.hasNext()){
				Authors tempP=iterateAuthors.next();
				String authorName1 = tempP.getName();
				if(authorName==null){
					flag=true;
					ArrayList<String> log = generateLog(tempP.getCommits(),sDate,eDate,authorName1);
					completeLog.addAll(log);
					}
				else if(authorName!=null && authorName1.equals(authorName)){				
					ArrayList<String> log = generateLog(tempP.getCommits(),sDate,eDate,authorName1);
					if(log.size()==0){
						System.out.println("For author:"+authorName+" no log found between the dates specified..");
						System.exit(0);
						}
					flag=true;
					completeLog.addAll(log);
					break;
					}
				}
		if(flag==false){
			System.out.println("Specified author:"+authorName+" not part of the given project log.");
			Runtime rt = Runtime.getRuntime();
			Process proc = rt.exec("rm -v output.log");
			proc.waitFor();
			System.exit(0);
			}
		else{
			Collections.sort(completeLog);
			writeLogToFile(completeLog,outputFile);
			outputFile.close();
			visualizeData(authorName,startDate,endDate);
			}
		}
		catch(ParseException e){
			e.printStackTrace();
			}
		}
	}