package com.cigital.svn.display.display2;
/**
 * <h1> Diff Parser </h1>
 * <p>It parses given diff and extracts required details of lines of code change</p>
 * 
 * @author breddy
 *
 */
class DiffParser {
	/**
	 * It parses given code and gives lines of code change information
	 * 
	 * @param code - diff
	 * @param locc - lines of code change object
	 * @return lines of code change object with updated information
	 */
	public LinesOfCodeChange parse(String code,LinesOfCodeChange locc)
	{
		if(code == null)
			return null;
		String lines[] = code.split("\n");
		for(int i=4;i<lines.length;i++){
			if(lines[i].length()<=0)
				continue;
			if(lines[i].charAt(0)=='+')
				locc.incrementAddedLinesCount();
			else if(lines[i].charAt(0)=='-')
				locc.incrementRemovedLinesCount();
			else
				continue;
		}
		return locc;
	}
}
