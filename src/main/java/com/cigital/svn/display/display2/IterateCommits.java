package com.cigital.svn.display.display2;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.cigital.svn.model.Diff;
import com.cigital.svn.model.Commit;
/**
 * 
 * <h1>Iterate over all commits</h1>
 * <p> This class iterates over all commits and generates corresponding lines of code change details
 * @author breddy
 *
 */
class IterateCommits {
	private ArrayList<ArrayList<LinesOfCodeChange>> commitsLocc = new ArrayList<ArrayList<LinesOfCodeChange>>();
	private ArrayList<Commit> commits=new ArrayList<Commit>();
	String authorName=null;
	/**
	 * This constructor assigns commitsLocc and authorName with corresponding values
	 * 
	 * @param commits - Set of commits (one author commits)
	 * @param authorName - user name
	 */
	IterateCommits(ArrayList<Commit> commits, String authorName){
		this.commits=commits;
		this.authorName = authorName;
	}
	/**
	 * It iterates over all commits of a specific author
	 * 
	 * @param start - start time
	 * @param end - end time
	 * @return commitsLocc - Commit wise lines of code change details for every diff
	 * @throws ParseException
	 */
	public ArrayList<ArrayList<LinesOfCodeChange>> iterateCommits(String start, String end) throws ParseException{

		ArrayList<LinesOfCodeChange> commitLocc = new ArrayList<LinesOfCodeChange>();
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
		Date startDate = sdf.parse(start);

			Date endDate = sdf.parse(end);

			for(int i=0;i<commits.size();i++){
				Date logTime =  sdf.parse(commits.get(i).getTimes());
				if((logTime.before(endDate) || logTime.equals(endDate)) && (logTime.after(startDate))||logTime.equals(startDate)){
					commitLocc = iterateDiffs(commits.get(i).getDifff(), commits.get(i).getTimes());
					commitsLocc.add(commitLocc);				
				}
		
		}
		
		return commitsLocc;
	}
	/**
	 * This iterates over all diffs of a particular commit
	 * 
	 * @param diffs - set of diffs of a single commit
	 * @param timeStamp - time sta,p
	 * @return - lines of code change details of a given commit's diffs
	 */
	public ArrayList<LinesOfCodeChange> iterateDiffs(ArrayList<Diff> diffs,String timeStamp){
		ArrayList<LinesOfCodeChange> commitLocc = new ArrayList<LinesOfCodeChange>();
		DiffParser df = new DiffParser();
		for(int i=0;i<diffs.size();i++){
			LinesOfCodeChange locc=new LinesOfCodeChange(diffs.get(i).getFiletype(),timeStamp,authorName);
			locc = df.parse(diffs.get(i).getCode(), locc);
			commitLocc.add(locc);
		}
		return commitLocc;	
	}
}






