package com.cigital.svn.display.display2;

import com.cigital.svn.model.Diff.Filetype;
/**
 * <h1> Lines of code change</h1>
 * <p> This class is template for lines of code change information</p>
 *
 * @author breddy
 * @since 04-06-2015
 */
class LinesOfCodeChange {

	private int addedLinesCount;
	private int removedLinesCount;
	Filetype fileType;
	String timeStamp;
	String authorName;
	/**
	 * This constructor initializes addedLinesCount, removedLinesCount = 0
	 * 
	 * @param fileType - File type(extension)
	 * @param timeStamp - time at which user added or removed code
	 * @param authorName - author name
	 */
	LinesOfCodeChange(Filetype fileType, String timeStamp, String authorName){
		addedLinesCount=0;
		removedLinesCount=0;
		this.fileType=fileType;
		this.timeStamp = timeStamp;
		this.authorName = authorName;
	}
	/**
	 * This method updates addedLinesCount
	 */
	public void incrementAddedLinesCount(){
		addedLinesCount++;
	}
	/**
	 * This method updates removedLinesCount
	 */
	public void incrementRemovedLinesCount(){
		removedLinesCount++;
	}
	/**
	 * Returns File Type of file which author used
	 * 
	 * @return fileType - file extension
	 */
	public Filetype getFileType(){
		return fileType;
	}
	/**
	 * Returns number of lines author added
	 * 
	 * @return addedLinesCount - Number of lines added by the author
	 */
	public int getAddedLinesCount(){
		return addedLinesCount;
	}
	/**
	 * Returns number of lines author removed
	 * 
	 * @return removedLinesCount - Number of lines removed by the author
	 */
		
	public int getRemovedLinesCount(){
		return removedLinesCount;
	}
	/**
	 * Returns author name
	 * 
	 * @return authorName - Name of the authors
	 */
	public String getAuthorName(){
		return authorName;
	}
	/**
	 * Returns TimeStamp
	 * 
	 * @return timeStamp - Time at which author accessed file
	 */
	public String getTimeStamp(){
		return timeStamp;
	}
}
