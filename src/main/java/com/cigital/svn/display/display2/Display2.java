package com.cigital.svn.display.display2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.cigital.svn.model.Authors;
import com.cigital.svn.model.Diff.Filetype;
/**
 * <h1> Display Lines of code change</h1>
 * <p>This class will displays the given author/all author log details by specifying which file type author had edited/deleted, lines of code added and lines of code deleted</p>
 *
 * @author breddy
 * @since 04-06-2015
 */
public class Display2 {
	ArrayList<ArrayList<LinesOfCodeChange>> commitsLocc = new ArrayList<ArrayList<LinesOfCodeChange>>();
	String authorName;
	/**
	 * This method calls and assigns commitsLocc variable value. This contains all diff's lines of code change information
	 * 
	 * @param authors - List of Author details
	 * @param author - Author name
	 * @param start - start time for verify log
	 * @param end - end time for verify log
	 * @throws ParseException
	 */
	public void display(ArrayList<Authors> authors, String author, String start, String end) throws ParseException{
		this.authorName=author;
		if(author==null){
			for(int i=0;i<authors.size();i++){
					IterateCommits ic = new IterateCommits(authors.get(i).getCommits(),authors.get(i).getName());
					commitsLocc=ic.iterateCommits(start, end);
					boolean noData=true;

					for(int ii=0;ii<commitsLocc.size();ii++){
						int added=0, removed=0;
						Filetype fileType=null;
						String time=null;
						for(int j=0;j<commitsLocc.get(ii).size();j++){
							added = commitsLocc.get(ii).get(j).getAddedLinesCount();
							removed = commitsLocc.get(ii).get(j).getRemovedLinesCount();
							fileType = commitsLocc.get(ii).get(j).getFileType();
							time = commitsLocc.get(ii).get(j).getTimeStamp();
							System.out.printf("%-10s %-10s %10s %3s %3s \n",commitsLocc.get(ii).get(j).getAuthorName(),fileType,time,added,removed);
							
							noData=false;
							}
						}
					if(noData)
						System.out.println("No data found");

			}

		}
		else{		

			for(int i=0;i<authors.size();i++){			

				if(authors.get(i).getName().trim().equals(author.trim())){
					IterateCommits ic = new IterateCommits(authors.get(i).getCommits(),authors.get(i).getName());
					commitsLocc=ic.iterateCommits(start, end);
					boolean noData=true;

					for(int ii=0;ii<commitsLocc.size();ii++){
						int added=0, removed=0;
						Filetype fileType=null;
						String time=null;
						for(int j=0;j<commitsLocc.get(ii).size();j++){
							added = commitsLocc.get(ii).get(j).getAddedLinesCount();
							removed = commitsLocc.get(ii).get(j).getRemovedLinesCount();
							fileType = commitsLocc.get(ii).get(j).getFileType();
							time = commitsLocc.get(ii).get(j).getTimeStamp();
							System.out.printf("%-10s %-10s %10s %3s %3s \n",commitsLocc.get(ii).get(j).getAuthorName(),fileType,time,added,removed);
							
							noData=false;
							}
						}
					if(noData)
						System.out.println("No data found");

				}
			}

		}
	}
}
