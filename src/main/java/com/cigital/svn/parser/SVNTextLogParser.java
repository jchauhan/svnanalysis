package com.cigital.svn.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;

import com.cigital.svn.model.Authors;
import com.cigital.svn.model.Commit;
import com.cigital.svn.model.Diff;
import com.cigital.svn.model.Diff.Filetype;
/**
 * 
 * @author bala
 *
 */

public class SVNTextLogParser {
	public ArrayList<Authors> SVNTextLogParser(String filename) throws IOException{
		String data=Read(filename);
		return parser(data);// check..
	}
	public static String Read(String val) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(new File(val)));
		String everything ="";
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append(System.lineSeparator());
	            line = br.readLine();
	        }
	        everything = sb.toString();
	    } finally {
	        br.close();
	    }
	    return everything;
	}
	public static ArrayList<Authors> parser(String input){
		HashMap<String,Authors> nameData=new HashMap<String, Authors>();
				String[] Data=input.split("------------------------------------------------------------------------");
				boolean firstNo=false;
				for(String s:Data){
					if(firstNo){
					String now[]=s.split("\n");
					int ind=0;
					boolean ok=false;
					for(;ind<now.length;ind++)
						if(now[ind].split("\\|").length==4){
						ok=true;
							break;
						}
					if(ok){
					String fields[]=now[ind].split("\\|");
					String tempAuthor=fields[1].trim();
					String tempId=fields[0];
					String tempDate=fields[2];
					tempDate=tempDate.trim().split("\\+")[0].trim().split(" ")[0].trim();
					/*Commit Class*/
					Commit temp_commit=new Commit();
					temp_commit.setRefno(tempId);
					temp_commit.setTimes(tempDate);
					
					for(int i=ind+1;i<now.length;i++){
						
						//Description of lines
								for(;i<now.length;i++){
									if(now[i].startsWith("Index:")){
										break;
									}
									else;
								}
						//diff Class path,code
								ArrayList<Diff> diff=new ArrayList<Diff>();
								Diff tempDiff = null;
								boolean firstTime=false;
								int indCount=0,creCount=0;
								for(;i<now.length;i++){
									if(now[i].startsWith("Index:")){
										if(firstTime){
											diff.add(tempDiff);
											creCount++;
										}
										indCount++;
										tempDiff=new Diff();
										tempDiff.setPath(now[i].substring(6));
										
										tempDiff.setFiletype(getFileType(now[i].substring(now[i].lastIndexOf(".")+1)));
										tempDiff.setCode(" ");
									}
									else{
										String str=tempDiff.getCode();
										tempDiff.setCode(str+"\n"+now[i]);
									//	System.out.println("(****"+tempDiff.getCode()+"\n(**)\n"+now[i]+"***)");
										firstTime=true;
									}
									/*if(indCount!=creCount)
									diff.add(tempDiff);*/
								}
								if(indCount!=creCount)
									diff.add(tempDiff);
							temp_commit.setDifff(diff);
					}
					if(nameData.containsKey(tempAuthor)){
						nameData.get(tempAuthor).getCommits().add(temp_commit);
					}
					else{
						/*Author Class*/
						Authors temp=new Authors();
						temp.setName(tempAuthor);
						ArrayList<Commit> commitsList=new ArrayList<Commit>();
						commitsList.add(temp_commit);
						temp.setCommits(commitsList);
						nameData.put(tempAuthor,temp);
					}
					}
				}
					else
						firstNo=true;
				}
				//Make Authors ArrayList
				ArrayList<Authors> returnAuthors=new ArrayList<Authors>();
				Iterator<String> k=nameData.keySet().iterator();
				while(k.hasNext()){
					returnAuthors.add(nameData.get(k.next()));
				}
		return returnAuthors;
	}
	private static Filetype getFileType(String fileType) {
		//System.out.println(fileType);
		// TODO Auto-generated method stub
		if(fileType.trim().equals("html")||fileType.trim().equals("htm"))
			return Filetype.html;
		else if(fileType.trim().equals("css"))
			return Filetype.css;
		else if(fileType.trim().equals("java"))
			return Filetype.java;
		else if(fileType.trim().equals("js"))
			return Filetype.javascript;
		else if(fileType.trim().equals("jquery"))
			return Filetype.jquery;
		else if(fileType.trim().equals("jsp"))
			return Filetype.jsp;
		else if(fileType.trim().equals("py"))
			return Filetype.python;
		else if(fileType.trim().equals("xml"))
			return Filetype.xml;
		else if(fileType.trim().equals("png"))
			return Filetype.png;
		else if(fileType.trim().equals("ico"))
			return Filetype.ico;
		else if(fileType.trim().equals("conf"))
			return Filetype.conf;
		return null;
	}
}