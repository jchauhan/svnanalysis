package com.cigital.svn.parser;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.cigital.svn.model.Authors;
import com.cigital.svn.model.Commit;
import com.cigital.svn.model.Diff;
import com.cigital.svn.model.Diff.Type;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
public class SvnXMLLogParser{
	private static Map<String,ArrayList<ArrayList<String>>> Complete_Data = new HashMap<String,ArrayList<ArrayList<String>>>();
	
	public ArrayList<Authors> convertToAuthorsList(Map<String,ArrayList<ArrayList<String>>> m)
	{
		ArrayList<Authors> authors = new ArrayList<Authors>();
		for(String key:m.keySet()){
			Authors a = new Authors();
			a.setName(key);
			ArrayList<Commit> commits = new ArrayList<Commit>(); 
			for(int i=0;i<m.get(key).size();i++)
			{
				ArrayList<String> commit = m.get(key).get(i);
				Commit c = new Commit();
				c.setTimes(commit.get(0));
				ArrayList<Diff> diff = new ArrayList<Diff>();
				Diff d = new Diff();
				d.setPath(commit.get(1));
				if(commit.get(2).equals("M"))
					d.setType(Type.update);
				else if(commit.get(2).equals("A"))
					d.setType(Type.add);
				else if(commit.get(2).equals("D"))
					d.setType(Type.delete);
				diff.add(d);
				c.setDifff(diff);
				commits.add(c);
			}
			a.setCommits(commits);
			authors.add(a);
		}
		return authors;
	}
	void Classify(ArrayList<String> A)throws Exception{
		String Author=A.get(0);
		if(Complete_Data.containsKey(Author)){
			ArrayList<ArrayList<String>> bl=Complete_Data.get(Author);
			ArrayList<String> cl=new ArrayList<String>();
			cl.add(A.get(1));cl.add(A.get(2));cl.add(A.get(3));
			bl.add(cl);
			Complete_Data.put(Author,bl);
			}
		else{
			ArrayList<ArrayList<String>> al=new ArrayList<ArrayList<String>>();
			ArrayList<String> cl=new ArrayList<String>();
			cl.add(A.get(1));cl.add(A.get(2));cl.add(A.get(3));
			al.add(cl);
			Complete_Data.put(Author,al);
			}
		}
	public ArrayList<Authors> xmlLogParse(String filename){
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse (new File(filename));            
            NodeList listOfPersons = doc.getElementsByTagName("logentry");
            for(int s=0; s<listOfPersons.getLength(); s++){
                Node firstPersonNode = listOfPersons.item(s);
                if(firstPersonNode.getNodeType() == Node.ELEMENT_NODE){
                    Element firstPersonElement = (Element)firstPersonNode;
					// Author name
                    NodeList authorNameList = firstPersonElement.getElementsByTagName("author");
                    Element author = (Element)authorNameList.item(0);
                    NodeList authorNames = author.getChildNodes();
                    String authorName=((Node)authorNames.item(0)).getNodeValue().trim();
					// Date 
					NodeList DatesList = firstPersonElement.getElementsByTagName("date");
					Element Modified_Date = (Element)DatesList.item(0);
					NodeList Modified_Date_List = Modified_Date.getChildNodes();
					String newDate = ((Node)Modified_Date_List.item(0)).getNodeValue().trim();
					// Paths
					NodeList PathsList = firstPersonElement.getElementsByTagName("path");
					for(int j=0;j<PathsList.getLength();j++){
						ArrayList<String> Data=new ArrayList<String>();
						Element Path_Entry = (Element)PathsList.item(j);
						String action = Path_Entry.getAttribute("action");
						NodeList filePath = Path_Entry.getChildNodes();							
						String filePath1 = ((Node)filePath.item(0)).getNodeValue().trim();
						Data.add(authorName);Data.add(newDate);Data.add(filePath1);Data.add(action);
						//fr.write(Data.toString()+"\n");
						Classify(Data); // Passing the array to classify them
						}
					}
				}
        }catch (SAXParseException err) {
			System.out.println ("** Parsing error " + ", line " + err.getLineNumber () + ", uri " + err.getSystemId ());
			System.out.println(" " + err.getMessage ());

        }catch (SAXException e) {
			Exception x = e.getException ();
				((x == null) ? e : x).printStackTrace ();
        }catch (Throwable t) {
			t.printStackTrace ();
        }
		return convertToAuthorsList(Complete_Data);
		}
	}
