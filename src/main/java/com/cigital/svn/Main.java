package  com.cigital.svn;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;



import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;

import com.cigital.svn.display.Display1;
import com.cigital.svn.display.display2.Display2;
import com.cigital.svn.display.visualize.Visualize;
import com.cigital.svn.model.Authors;
import com.cigital.svn.parser.SVNTextLogParser;
import com.cigital.svn.parser.SvnXMLLogParser;
/**
 * 
 * @author bala
 *
 */
public class Main {
	
	public static String authorName = null,fromDate= null,toDate = null,path=null;
	public static String display;
	public static void main(String args[]) throws ParseException{
		try{
		ReadCommandLineParameter(args);
		ArrayList<Authors> authorsList = null;
		if(path.endsWith(".diff")){//TextLogParser
			try {
				authorsList=new SVNTextLogParser().SVNTextLogParser(path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else if(path.endsWith(".xml")){///XMLlogParser
			authorsList = new SvnXMLLogParser().xmlLogParse(path);
		}
		else{
			System.out.println("-f,--file <arg>         [mandatory] file path format /directory/filename.diff and file type .diff or .xml Ex. -f /directory/input.diff ");
		}
		
		//display1
		if(display.equals("fc")){
			boolean validate=validateValues(fromDate,toDate);
			if(validate){
			Display1 display=new Display1();
			display.Display1(authorsList, authorName, fromDate, toDate);}
		}
		else if(display.equals("locc")){
			if(path.endsWith(".xml")){
				System.out.println("Display2 is not applicable for xml format");
				System.exit(0);
			}
			Display2 display = new Display2();
			if(fromDate==null)
				fromDate="2005-01-01";
			if(toDate==null)
			{
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, 1);
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
				String formatted = format1.format(cal.getTime());
				toDate = formatted;
			}
				
			display.display(authorsList, authorName, fromDate, toDate);
		}
		else if(display.equals("visualize")){
			if(path.endsWith(".diff")){
				System.out.println("Visualize is not applicable for diff format. It only works on xml format.");
				System.exit(0);
			}
			else{
				Visualize obj = new Visualize();
				obj.visualizePerformance(authorsList, authorName, fromDate, toDate);
			}
		}
		else{
			System.out.println("-d,--display <arg>      [mandatory] display format is fc or locc.fc for filename and code. locc for line of code change and code Ex. -d fc");
		}
		}catch(Exception e){}
	}

	private static boolean validateValues(String fromDate2, String toDate2) {
		if(fromDate2==null&&toDate2==null)
			return true;
		else if(fromDate2==null&&dateChecker(toDate2))
			return true;
		else if(dateChecker(fromDate2)&&toDate2==null)
			return true;
		else if(dateChecker(fromDate2)&&dateChecker(toDate2)){
			return true;
		}
		else{
			if(dateChecker(fromDate2)){
				System.out.println("-et,--endtime <arg>     end time format is yyyy:mm:dd Ex. -et 2014-07-08");
			}
			else if(dateChecker(toDate2)){
				System.out.println("-st,--starttime <arg>   start time format is yyyy:mm:dd  Ex. -et 2014-07-08");
			}
			else{
				System.out.println("-et,--endtime <arg>     end time format is yyyy:mm:dd Ex. -et 2014-07-08");
				System.out.println("-st,--starttime <arg>   start time format is yyyy:mm:dd  Ex. -et 2014-07-08");
			}
			return false;
		}
	}

	private static boolean dateChecker(String date) {
		try{
		String arr[]=date.split("-");
		if(arr.length==3){
			Integer.parseInt(arr[0].trim());
			Integer.parseInt(arr[1].trim());
			Integer.parseInt(arr[2].trim());
			return true;
		}
		else
			return false;
		}catch(Exception e){
		return false;
		}
	}

	private static void ReadCommandLineParameter(String[] args) {
		CommandLineParser parser=new BasicParser();
		Options options=new Options();
		options.addOption("u","user",true,"Author name format is name \n Ex. -u ayesha \n");
		options.addOption("st","starttime",true,"start time format is yyyy:mm:dd \n Ex. -et 2014-07-08 \n");
		options.addOption("et","endtime",true,"end time format is yyyy:mm:dd \n Ex. -et 2014-07-08 \n");
		options.addOption("h","help",false,"Shows Help \n");
		options.addOption("f","file",true,"[mandatory] file path format /directory/filename.diff and file type xml or diff \n Ex. -f /directory/input.diff \n");
		options.addOption("d","display",true,"[mandatory] display format is fc or locc.\n fc for filename and code .\n locc for line of code change and code \n visualize for visualizing log(default log format .xml only) Ex. -d fc \n");
		CommandLine command;
		try {
			command = parser.parse(options,args);
			authorName=command.getOptionValue("u");
			fromDate=command.getOptionValue("st");
			toDate=command.getOptionValue("et");
			path=command.getOptionValue("f");
			try{
			display=(command.getOptionValue("d")).trim();
			}catch(Exception e){display=null;}
			if(command.hasOption("h")){
				HelpFormatter format=new HelpFormatter();
				format.printHelp("Show Options Usage", options);
			}
		} catch (org.apache.commons.cli.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
